-- See the file "LICENSE" for the full license governing this code. --

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.lt16x32_internal.all;
use work.lt16x32_global.all;
use work.wishbone.all;
use work.config.all;

package lt16soc_peripherals is
	component lt16soc_top
		generic(
			programfilename : string := "programs/blinky.ram"
		);
		port(
			clk           : in    std_logic;
			rst           : in    std_logic;
			led           : out   std_logic_vector(7 downto 0);
			sw            : in    std_logic_vector(7 downto 0);
			btn           : in    std_logic_vector(6 downto 0);
			dataLCD       : INOUT std_logic_vector(7 downto 0);
			enableLCD     : OUT   std_logic;
			rsLCD         : OUT   std_logic;
			rwLCD         : OUT   std_logic;
			ps2_keyb_clk  : inout std_logic;
			ps2_keyb_data : inout std_logic;
			driver_en     : out   std_logic;
			receive_en    : out   std_logic;
			rxd1          : in    std_logic;
			txd1          : out   std_logic
		);
	end component lt16soc_top;

	component wb_led is
		generic(
			memaddr  : generic_addr_type; -- := CFG_BADR_LED;
			addrmask : generic_mask_type -- := CFG_MADR_LED;
		);
		port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			led   : out std_logic_vector(7 downto 0);
			wslvi : in  wb_slv_in_type;
			wslvo : out wb_slv_out_type
		);
	end component;

	component wb_switch
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			btn   : in  std_logic_vector(6 downto 0);
			sw    : in  std_logic_vector(7 downto 0);
			wslvi : in  wb_slv_in_type;
			wslvo : out wb_slv_out_type
		);
	end component wb_switch;

	component wb_timer
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk       : in  std_logic;
			rst       : in  std_logic;
			interrupt : out std_logic;
			wslvi     : in  wb_slv_in_type;
			wslvo     : out wb_slv_out_type
		);
	end component wb_timer;

	component wb_lcd
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk       : IN    std_logic;
			rst       : IN    std_logic;
			wslvi     : IN    wb_slv_in_type;
			wslvo     : OUT   wb_slv_out_type;
			dataLCD   : INOUT std_logic_vector(7 downto 0);
			enableLCD : OUT   std_logic;
			rsLCD     : OUT   std_logic;
			rwLCD     : OUT   std_logic
		);
	end component wb_lcd;

	component wb_lcd_adv
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk       : IN    std_logic;
			rst       : IN    std_logic;
			wslvi     : IN    wb_slv_in_type;
			wslvo     : OUT   wb_slv_out_type;
			dataLCD   : INOUT std_logic_vector(7 downto 0);
			enableLCD : OUT   std_logic;
			rsLCD     : OUT   std_logic;
			rwLCD     : OUT   std_logic
		);
	end component wb_lcd_adv;

	component wb_ps2
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk           : in    std_logic;
			rst           : in    std_logic;
			wslvi         : in    wb_slv_in_type;
			wslvo         : out   wb_slv_out_type;
			ascii_new     : OUT   STD_LOGIC;
			ps2_keyb_clk  : inout std_logic;
			ps2_keyb_data : inout std_logic
		);
	end component wb_ps2;

	component can_vhdl_top
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk        : in  std_logic;
			rstn       : in  std_logic;
			wbs_i      : in  wb_slv_in_type;
			wbs_o      : out wb_slv_out_type;
			rx_i       : in  std_logic;
			tx_o       : out std_logic;
			driver_en  : out std_logic;
			receive_en : out std_logic;
			irq_on     : out std_logic
		);
	end component can_vhdl_top;

end lt16soc_peripherals;

package body lt16soc_peripherals is

--insert function bodies

end lt16soc_peripherals;

