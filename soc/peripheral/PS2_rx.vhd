library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PS2_rx is
	port(
		clk              : in  std_logic; -- 100MHz
		rst              : in  std_logic; -- active HIGH

		ps2d             : in  std_logic; -- PS/2 data line
		ps2c             : in  std_logic; -- PS/2 clock line

		rx_en            : in  std_logic; -- receive enable
		parity_fail_flag : out std_logic; -- parity flag
		rx_done_flag     : out std_logic; -- receive done flag
		dout             : out std_logic_vector(7 downto 0) -- 1 byte data out after receiving full byte
	);
end entity PS2_rx;

architecture RTL of PS2_rx is
	type statetype is (idle, dps, load);
	signal state_reg, state_next                 : statetype;
	signal filter_reg, filter_next               : std_logic_vector(7 downto 0);
	signal filtered_ps2c_reg, filtered_ps2c_next : std_logic;
	signal rx_data_reg, rx_data_next             : std_logic_vector(10 downto 0);
	signal bit_counter_reg, bit_counter_next     : unsigned(3 downto 0);
	signal fall_edge                             : std_logic;
	signal comp_parity                           : std_logic;
	signal recv_parity                           : std_logic;
begin
	--=================================================
	-- filter and falling edge tick generation for ps2c
	--=================================================
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				filter_reg        <= (others => '0');
				filtered_ps2c_reg <= '0';
			else
				filter_reg        <= filter_next;
				filtered_ps2c_reg <= filtered_ps2c_next;
			end if;
		end if;
	end process;

	filter_next        <= ps2c & filter_reg(7 downto 1);
	filtered_ps2c_next <= '1' when filter_reg = "11111111" else '0' when filter_reg = "00000000" else filtered_ps2c_reg;
	fall_edge          <= filtered_ps2c_reg and (not filtered_ps2c_next);

	--=================================================
	-- fsmd to extract the 8-bit data
	--=================================================
	-- registers
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				state_reg       <= idle;
				bit_counter_reg <= (others => '0');
				rx_data_reg     <= (others => '0');
			else
				state_reg       <= state_next;
				bit_counter_reg <= bit_counter_next;
				rx_data_reg     <= rx_data_next;
			end if;
		end if;
	end process;

	-- next-state logic
	process(state_reg, bit_counter_reg, rx_data_reg, fall_edge, rx_en, ps2d)
	begin
		rx_done_flag     <= '0';
		state_next       <= state_reg;
		bit_counter_next <= bit_counter_reg;
		rx_data_next     <= rx_data_reg;
		case state_reg is
			when idle =>
				if fall_edge = '1' and rx_en = '1' then
					-- shift in start bit
					rx_data_next     <= ps2d & rx_data_reg(10 downto 1);
					bit_counter_next <= "1001";
					state_next       <= dps;
				end if;
			when dps =>                 -- 8 data + 1 pairty + 1 stop
				if fall_edge = '1' then
					rx_data_next <= ps2d & rx_data_reg(10 downto 1);
					bit_counter_next <= bit_counter_reg - 1;
					if bit_counter_reg = 0 then
						state_next <= load;
					end if;
				end if;
			when load =>
				-- 1 extra clock to complete the last shift
				state_next   <= idle;
				rx_done_flag <= '1';
		end case;
	end process;
	-- output
	dout             <= rx_data_reg(8 downto 1); -- data bits
	comp_parity      <= not (rx_data_reg(8) xor rx_data_reg(7) xor rx_data_reg(6) xor rx_data_reg(5) xor rx_data_reg(4) xor rx_data_reg(3) xor rx_data_reg(2) xor rx_data_reg(1));
	recv_parity      <= rx_data_reg(9);
	parity_fail_flag <= comp_parity xor recv_parity;
end architecture RTL;
