-- See the file "LICENSE" for the full license governing this code. --

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lt16x32_global.all;
use work.wishbone.all;
use work.config.all;

entity wb_timer is
	generic(
		memaddr  : generic_addr_type;   --:= CFG_BADR_LED;
		addrmask : generic_mask_type    --:= CFG_MADR_LED;
	);
	port(
		clk       : in  std_logic;
		rst       : in  std_logic;
		interrupt : out std_logic;
		wslvi     : in  wb_slv_in_type;
		wslvo     : out wb_slv_out_type
	);
end wb_timer;

architecture Behavioral of wb_timer is
	signal counter        : unsigned(31 downto 0);
	signal target_counter : std_logic_vector(31 downto 0);
	signal control_flag   : std_logic_vector(31 downto 0);
	signal ack            : std_logic;
	alias enable is control_flag(2);
	alias repeat is control_flag(1);
	alias reset is control_flag(0);
begin
	interrupt <= '1' when ((counter = unsigned(target_counter)) and (unsigned(target_counter) /= 0)) else '0';

	counter_proc : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' or reset = '1' then
				counter <= (others => '0');
			else
				if (enable = '1') then
					counter <= counter + 1;
					if (counter = unsigned(target_counter)) then
						counter <= (others => '0');
					end if;
				end if;
			end if;
		end if;
	end process counter_proc;

	write_proc : process(clk)
	begin
		if clk'event and clk = '1' then
			if rst = '1' then
				ack            <= '0';
				target_counter <= (others => '0');
				control_flag   <= (others => '0');
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '1' then
						if wslvi.adr(2) = '0' then
							target_counter <= dec_wb_dat(wslvi.sel, wslvi.dat);
						elsif wslvi.adr(2) = '1' then
							control_flag <= dec_wb_dat(wslvi.sel, wslvi.dat);
						end if;
					end if;
					ack <= not ack;
				else
					ack <= '0';
				end if;
				if (counter = unsigned(target_counter) and repeat = '0') then
					enable <= '0';
				end if;
			end if;
		end if;
	end process write_proc;

	read_proc : process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				ack <= '0';
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '0' then
						if wslvi.adr(2) = '0' then
							wslvo.dat <= target_counter;
						elsif wslvi.adr(2) = '1' then
							wslvo.dat <= control_flag(31 downto 1) & '0';
						end if;
					end if;
					ack <= not ack;
				else
					ack <= '0';
				end if;
			end if;
		end if;
	end process read_proc;

	wslvo.ack   <= ack;
	wslvo.wbcfg <= wb_membar(memaddr, addrmask);

end Behavioral;