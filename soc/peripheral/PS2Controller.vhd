library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.lt16x32_global.all;
USE work.wishbone.all;
USE work.config.all;
use work.lt16soc_utils.all;

entity wb_ps2 is
	generic(
		memaddr  : generic_addr_type;
		addrmask : generic_mask_type
	);

	port(
		clk           : in    std_logic;
		rst           : in    std_logic;
		wslvi         : in    wb_slv_in_type;
		wslvo         : out   wb_slv_out_type;
		ascii_new     : OUT   STD_LOGIC; --output flag indicating new ASCII value, for interrupt
		ps2_keyb_clk  : inout std_logic;
		ps2_keyb_data : inout std_logic
	);
end entity wb_ps2;

architecture RTL of wb_ps2 is

	-- components
	component PS2_rxtx is
		port(
			clk              : in    std_logic;
			rst              : in    std_logic;

			-- transmision port
			tx_en            : in    std_logic; -- transmit enable
			din              : in    std_logic_vector(7 downto 0);
			tx_done_tick     : out   std_logic;

			-- reception port 
			dout             : out   std_logic_vector(7 downto 0);
			rx_done_tick     : out   std_logic;
			parity_fail_flag : out   std_logic;

			-- PS2 interfaces
			ps2d, ps2c       : inout std_logic
		);
	end component PS2_rxtx;

	-- transmision signal
	signal tx_en        : std_logic;    -- transmit enable
	signal din          : std_logic_vector(7 downto 0);
	signal tx_done_tick : std_logic;

	-- reception signal 
	signal ps2_code         : std_logic_vector(7 downto 0);
	signal ps2_code_new     : std_logic;
	signal parity_fail_flag : std_logic;

	-- wishbone interface signals
	signal ack        : std_logic;
	signal ascii      : std_logic_vector(7 downto 0);
	signal read_ascii : std_logic_vector(7 downto 0);
	signal write_reg  : std_logic_vector(7 downto 0);

	-- states
	TYPE machine IS (ready, new_code, translate, output); --needed states
	SIGNAL state : machine;             --state machine

	-- others
	--  SIGNAL prev_ps2_code_new : STD_LOGIC := '1'; --value of ps2_code_new flag on previous clock
	SIGNAL break   : STD_LOGIC;         --'1' for break code, '0' for make code
	SIGNAL shift_r : STD_LOGIC;         --'1' if right shift is held down, else '0'
	SIGNAL shift_l : STD_LOGIC;         --'1' if left shift is held down, else '0'

begin
	PROCESS(clk)
	BEGIN
		IF (clk'EVENT AND clk = '1') THEN
			if rst = '1' then
				state      <= ready;
				read_ascii <= (others => '0');
				ascii      <= (others => '0');
				break      <= '0';
				shift_r    <= '0';
				shift_l    <= '0';
			else
				ascii_new <= '0';       --reset new ASCII code indicator
				CASE state IS
					-- ready state: wait for a new PS2 code to be received
					WHEN ready =>
						IF (ps2_code_new = '1') THEN
							-- new PS2 code received
							ascii_new <= '0'; --reset new ASCII code indicator
							state     <= new_code; --proceed to new_code state
						ELSE
							-- no new PS2 code received yet
							state <= ready; --remain in ready state
						END IF;

					-- new_code state: determine what to do with the new PS2 code
					WHEN new_code =>
						IF (ps2_code = x"F0") THEN
							-- code indicates that next command is break
							break <= '1'; --set break flag
							state <= ready; --return to ready state to await next PS2 code
						ELSIF (ps2_code = x"E0") THEN
							-- code indicates multi-key command
							-- we DONOT implement processing mechanism for extended key
							state   <= ready; --return to ready state to await next PS2 code
						ELSE
							-- code is the last PS2 code in the make/break code
							ascii(7) <= '1'; -- set internal ascii value to unsupported code (for verification)
							state    <= translate; -- proceed to translate state
						END IF;

					-- translate state: translate PS2 code to ASCII value
					WHEN translate =>
						break <= '0';   --reset break flag

						--handle codes for control, shift, and caps lock
						CASE ps2_code IS
							WHEN x"12" =>
								-- left shift code
								shift_l <= NOT break; --update left shift flag
							WHEN x"59" =>
								-- right shift code
								shift_r <= NOT break; --update right shift flag
							WHEN OTHERS => NULL;
						END CASE;

						IF (break = '0') THEN --the code is a make
							IF (shift_l = '1' OR shift_r = '1') THEN
								-- key's secondary character is desired
								ascii <= to_upper_ascii(ps2_code);
							ELSE
								-- key's primary character is desired
								ascii <= to_lower_ascii(ps2_code);
							END IF;
							state <= output; --proceed to output state
						ELSE            
							-- code is a break
							state <= ready; --return to ready state to await next PS2 code
						END IF;

					-- output state: verify the code is valid and output the ASCII value
					WHEN output =>
						IF (ascii(7) = '0') THEN
							-- the PS2 code has an ASCII output
							ascii_new  <= '1'; --set flag indicating new ASCII output
							read_ascii <= ascii(7 DOWNTO 0); --output the ASCII value
						END IF;
						state <= ready; --return to ready state to await next PS2 code
				END CASE;
			end if;
		END IF;
	END PROCESS;

	ps2_rxtx_unit : PS2_rxtx port map(
			clk              => clk,
			rst              => rst,
			tx_en            => tx_en,
			din              => din,
			tx_done_tick     => tx_done_tick,
			dout             => ps2_code,
			rx_done_tick     => ps2_code_new,
			parity_fail_flag => parity_fail_flag,
			ps2d             => ps2_keyb_data,
			ps2c             => ps2_keyb_clk
		);

	din <= write_reg(7 downto 0);

	read_write_proc : process(clk)
	begin
		if clk'event and clk = '1' then
			tx_en <= '0';
			if rst = '1' then
				ack                     <= '0';
				write_reg               <= (others => '0');
				wslvo.dat(31 downto 16) <= (others => '0');
			else
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we = '1' then
						write_reg <= dec_wb_dat(wslvi.sel, wslvi.dat)(7 downto 0);
						tx_en     <= '1';
						ack       <= not ack;
					elsif wslvi.we = '0' then
						wslvo.dat(31 downto 16) <= read_ascii & parity_fail_flag & ps2_code_new & tx_done_tick & "00000";
						ack                     <= not ack;
					else
						ack <= '0';
					end if;
				else
					ack <= '0';
				end if;
			end if;
		end if;
	end process read_write_proc;

	wslvo.dat(15 downto 0) <= (others => '0');
	wslvo.wbcfg            <= wb_membar(memaddr, addrmask);
	wslvo.ack              <= ack;
end architecture RTL;
