library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PS2_rxtx is
	port(
		clk              : in    std_logic;
		rst              : in    std_logic;

		-- transmision port
		tx_en            : in    std_logic; -- transmit enable
		din              : in    std_logic_vector(7 downto 0);
		tx_done_tick     : out   std_logic;

		-- reception port	
		dout             : out   std_logic_vector(7 downto 0);
		rx_done_tick     : out   std_logic;
		parity_fail_flag : out   std_logic;

		-- PS2 interfaces
		ps2d, ps2c       : inout std_logic
	);
end entity PS2_rxtx;

architecture RTL of PS2_rxtx is
	component PS2_rx is
		port(
			clk              : in  std_logic; -- 100MHz
			rst              : in  std_logic; -- active HIGH

			ps2d             : in  std_logic; -- PS/2 data line
			ps2c             : in  std_logic; -- PS/2 clock line

			rx_en            : in  std_logic; -- receive enable
			parity_fail_flag : out std_logic; -- parity flag
			rx_done_flag     : out std_logic; -- receive done flag
			dout             : out std_logic_vector(7 downto 0) -- 1 byte data out after receiving full byte
		);
	end component PS2_rx;

	component PS2_tx is
		port(
			clk          : in    std_logic; -- 100MHz
			rst          : in    std_logic; -- active HIGH

			din          : in    std_logic_vector(7 downto 0); -- 1 byte data to be sent
			tx_en        : in    std_logic; -- transmit enable

			ps2d         : inout std_logic;
			ps2c         : inout std_logic;
			tx_idle_flag : out   std_logic; -- idle flag, indicate TX module is in idle stage
			tx_done_flag : out   std_logic -- transmision done flag

		);
	end component PS2_tx;

	signal tx_idle : std_logic;

begin
	ps2_tx_unit : PS2_tx port map(
			clk          => clk,
			rst          => rst,
			din          => din,
			tx_en        => tx_en,
			ps2d         => ps2d,
			ps2c         => ps2c,
			tx_idle_flag => tx_idle,
			tx_done_flag => tx_done_tick
		);

	ps2_rx_unit : PS2_rx port map(
			clk              => clk,
			rst              => rst,
			ps2d             => ps2d,
			ps2c             => ps2c,
			rx_en            => tx_idle,
			parity_fail_flag => parity_fail_flag,
			rx_done_flag     => rx_done_tick,
			dout             => dout
		);
end architecture RTL;
