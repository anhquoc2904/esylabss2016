library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PS2_tx is
	port(
		clk          : in    std_logic; -- 100MHz
		rst          : in    std_logic; -- active HIGH

		din          : in    std_logic_vector(7 downto 0); -- 1 byte data to be sent
		tx_en        : in    std_logic; -- transmit enable

		ps2d         : inout std_logic;
		ps2c         : inout std_logic;
		tx_idle_flag : out   std_logic; -- idle flag, indicate TX module is in idle stage
		tx_done_flag : out   std_logic  -- transmision done flag

	);
end entity PS2_tx;

architecture RTL of PS2_tx is
	type statetype is (idle, rts, start, data, stop);
	signal state_reg, state_next                 : statetype;
	signal filter_reg, filter_next               : std_logic_vector(7 downto 0);
	signal filtered_ps2c_reg, filtered_ps2c_next : std_logic;
	signal fall_edge                             : std_logic;
	signal tx_data_reg, tx_data_next             : std_logic_vector(8 downto 0);
	signal rts_counter_reg, rts_counter_next     : unsigned(13 downto 0);
	signal bit_counter_reg, bit_counter_next     : unsigned(3 downto 0);
	signal parity                                : std_logic;
	signal ps2c_out, ps2d_out                    : std_logic;
	signal tri_c, tri_d                          : std_logic;
begin
	--=================================================
	-- filter and falling edge tick generation for ps2c
	--=================================================
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				filter_reg        <= (others => '0');
				filtered_ps2c_reg <= '0';
			else
				filter_reg        <= filter_next;
				filtered_ps2c_reg <= filtered_ps2c_next;
			end if;
		end if;
	end process;

	filter_next        <= ps2c & filter_reg(7 downto 1);
	filtered_ps2c_next <= '1' when filter_reg = "11111111" else '0' when filter_reg = "00000000" else filtered_ps2c_reg;
	fall_edge          <= filtered_ps2c_reg and (not filtered_ps2c_next);

	--=================================================
	-- fsmd
	--=================================================
	-- registers
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				state_reg       <= idle;
				rts_counter_reg <= (others => '0');
				bit_counter_reg <= (others => '0');
				tx_data_reg     <= (others => '0');
			else
				state_reg       <= state_next;
				rts_counter_reg <= rts_counter_next;
				bit_counter_reg <= bit_counter_next;
				tx_data_reg     <= tx_data_next;
			end if;
		end if;
	end process;
	-- odd parity bit
	parity <= not (din(7) xor din(6) xor din(5) xor din(4) xor din(3) xor din(2) xor din(1) xor din(0));
	-- next-state logic
	process(state_reg, bit_counter_reg, tx_data_reg, rts_counter_reg, tx_en, din, parity, fall_edge)
	begin
		state_next       <= state_reg;
		rts_counter_next <= rts_counter_reg;
		bit_counter_next <= bit_counter_reg;
		tx_data_next     <= tx_data_reg;
		tx_done_flag     <= '0';
		ps2c_out         <= '1';
		ps2d_out         <= '1';
		tri_c            <= '0';
		tri_d            <= '0';
		tx_idle_flag     <= '0';
		case state_reg is
			when idle =>
				tx_idle_flag <= '1';
				if tx_en = '1' then
					tx_data_next     <= parity & din;
					rts_counter_next <= (others => '1'); -- 2^13-1
					state_next       <= rts;
				end if;
			when rts =>                 -- request to send
				ps2c_out         <= '0';
				tri_c            <= '1';
				rts_counter_next <= rts_counter_reg - 1;
				if (rts_counter_reg = 0) then
					state_next <= start;
				end if;
			when start =>               -- assert start bit
				ps2d_out <= '0';
				tri_d    <= '1';
				if fall_edge = '1' then
					bit_counter_next <= "1000";
					state_next       <= data;
				end if;
			when data =>                -- 8 data + 1 pairty
				ps2d_out <= tx_data_reg(0);
				tri_d    <= '1';
				if fall_edge = '1' then
					tx_data_next <= '0' & tx_data_reg(8 downto 1);
					if bit_counter_reg = 0 then
						state_next <= stop;
					else
						bit_counter_next <= bit_counter_reg - 1;
					end if;
				end if;
			when stop =>                -- assume floating high for ps2d
				if fall_edge = '1' then
					state_next   <= idle;
					tx_done_flag <= '1';
				end if;
		end case;
	end process;

	-- tri-state buffers
	ps2c <= ps2c_out when tri_c = '1' else 'Z';
	ps2d <= ps2d_out when tri_d = '1' else 'Z';

end architecture RTL;
