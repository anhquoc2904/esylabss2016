library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lt16soc_peripherals.all;

entity lcdctrl_top_tb is
end entity lcdctrl_top_tb;

architecture RTL of lcdctrl_top_tb is
	constant CLK_PERIOD : time := 10 ns;
	signal pre_time     : time := -10 ns;

	signal clk : std_logic := '0';
	signal rst : std_logic;

	signal sw  : std_logic_vector(7 downto 0);
	signal btn : std_logic_vector(6 downto 0);
	signal led : std_logic_vector(7 downto 0);

	signal dataLCD   : std_logic_vector(7 downto 0);
	signal enableLCD : std_logic;
	signal rsLCD     : std_logic;
	signal rwLCD     : std_logic;

begin
	lcdctrldev : lt16soc_top
		generic map(
			programfilename => "programs/assignment31code.ram"
		)
		port map(
			clk           => clk,
			rst           => rst,
			led           => led,
			sw            => sw,
			btn           => btn,
			dataLCD       => dataLCD,
			enableLCD     => enableLCD,
			rsLCD         => rsLCD,
			rwLCD         => rwLCD,
			ps2_keyb_clk  => open,
			ps2_keyb_data => open,
			driver_en     => open,
			receive_en    => open,
			rxd1          => open,
			txd1          => open
		);

	time_proc : process(clk)
	begin
		if rising_edge(clk) then
			pre_time <= pre_time + 10 ns;
		end if;
	end process time_proc;

	clk_gen : process
	begin
		clk <= not clk;
		wait for CLK_PERIOD / 2;
	end process clk_gen;

	reset_gen : process
	begin
		rst <= '0';
		wait for CLK_PERIOD;
		rst <= '1';
		wait for 20000000 * CLK_PERIOD;
	end process reset_gen;

--	time_check : process
--	begin
--		power_time <= pre_time;
--		wait until (dataLCD = X"38") for 10 us;
--		funcset_time <= pre_time;
--		assert funcset_time < (power_time + 40 ms) report "Time require before function set missed" severity ERROR;
--
--		wait until (dataLCD = X"0F") for 10 us;
--		displayON_time <= pre_time;
--		assert (displayON_time < funcset_time + 37 us) report "Time require before display ON/OFF missed" severity ERROR;
--
--		wait until (dataLCD = X"01") for 10 us;
--		displayClear_time <= pre_time;
--		assert (displayClear_time < displayON_time + 37 us) report "Time require before display clear missed" severity ERROR;
--
--		wait until (dataLCD = X"06") for 10 us;
--		setEntryMode_time <= pre_time;
--		assert (setEntryMode_time < displayClear_time + 1.5 ms) report "Time require before set Entry Mode missed" severity ERROR;
--	end process time_check;

end architecture RTL;
