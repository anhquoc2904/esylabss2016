library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone.all;
use work.can_tp.all;
use work.config.all;

entity can_tx_sim_tb is
end entity can_tx_sim_tb;

architecture RTL of can_tx_sim_tb is
	component can_vhdl_top is
		generic(
			memaddr  : generic_addr_type;
			addrmask : generic_mask_type
		);
		port(
			clk    : in  std_logic;
			rstn   : in  std_logic;
			wbs_i  : in  wb_slv_in_type;
			wbs_o  : out wb_slv_out_type;
			rx_i   : in  std_logic;
			tx_o   : out std_logic;
			irq_on : out std_logic
		);
	end component can_vhdl_top;

	component phys_can_sim
		generic(
			peer_num : integer
		);
		port(
			rst       : in  std_logic;
			rx_vector : out std_logic_vector(peer_num - 1 downto 0);
			tx_vector : in  std_logic_vector(peer_num - 1 downto 0));
	end component phys_can_sim;

	-- management signal
	signal clk         : std_logic := '0';
	signal rst         : std_logic := '1';
	signal test_result : rx_check_result;
	signal tx_frame    : std_logic_vector(0 to 108);

	-- signals main controller
	signal wbs_i  : wb_slv_in_type;
	signal wbs_o  : wb_slv_out_type;
	signal irq_on : std_logic;

	--signals can interconnect
	constant peer_num_inst : integer := 2;
	signal rx_vector       : std_logic_vector(peer_num_inst - 1 downto 0);
	signal tx_vector       : std_logic_vector(peer_num_inst - 1 downto 0);

begin
	can_inst_main : component can_vhdl_top
		generic map(
			memaddr  => CFG_BADR_MEM,
			addrmask => CFG_MADR_FULL
		)
		port map(
			clk    => clk,
			rstn   => rst,
			wbs_i  => wbs_i,
			wbs_o  => wbs_o,
			rx_i   => rx_vector(0),
			tx_o   => tx_vector(0),
			irq_on => irq_on);

	can_interconnect : component phys_can_sim
		generic map(peer_num => peer_num_inst)
		port map(rst       => rst,
			     rx_vector => rx_vector,
			     tx_vector => tx_vector);

	-- stimuli
	stimuli : process is
	--variable data : std_logic_vector (7 downto 0);
	--variable addr : integer;
	begin
		wait for 10 ns;
		rst <= '1';
		wait for 40 ns;
		rst <= '0';

		tx_vector(1) <= '1';
		write_regs_from_file("./testdata/default_setup.tdf", wbs_i, wbs_o, clk);
		wait for 8400 ns;
		simulate_can_transmission("10101010111", x"770F0F0F00000000", 1, 300 ns, rx_vector(1), tx_vector(1), test_result);
		wait for 2400 ns;
		simulate_can_transmission("11100010111", x"770F0F0F00000000", 3, 300 ns, rx_vector(1), tx_vector(1), test_result);
		wait for 8400 ns;
		simulate_can_transmission("00000000011", x"770F0F0F00000000", 5, 300 ns, rx_vector(1), tx_vector(1), test_result);
		wait for 8400 ns;
		simulate_can_transmission("10111110011", x"770F0F0F00000000", 8, 300 ns, rx_vector(1), tx_vector(1), test_result);
		tx_vector(1) <= '1';
		wait;

		report "end stimuli" severity warning;

	end process stimuli;

	sub_programm_test : process is
	begin
		tx_frame <= buildframe("00000000000", x"770F0F0F00000000", 1);
		wait;
	end process;

	-- clock generation
	clock : process is
	begin
		clk <= not clk;
		wait for 10 ns / 2;
	end process clock;

end architecture RTL;
