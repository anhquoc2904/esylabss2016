-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;

ENTITY timer_tb IS
END ENTITY;

ARCHITECTURE sim OF timer_tb IS
	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic := '0';
	signal rst : std_logic;

	signal interrupt  : std_logic;
	--	signal write_target_counter : std_logic_vector(31 downto 0) := (others => '0');
	--	signal write_control_flags : std_logic_vector(31 downto 0) := (others => '0');
	signal writedata  : std_logic_vector(31 downto 0) := (others => '0');
	signal addroffset : integer;

	signal slvi : wb_slv_in_type;
	signal slvo : wb_slv_out_type;
	signal msto : wb_mst_out_type;

BEGIN
	slvi.adr <= msto.adr;
	slvi.dat <= msto.dat;
	slvi.bte <= msto.bte;
	slvi.we  <= msto.we;
	slvi.sel <= msto.sel;
	slvi.stb <= msto.stb;
	slvi.cyc <= msto.cyc;
	slvi.cti <= msto.cti;

	SIM_SLV : wb_timer
		generic map(
			memaddr  => CFG_BADR_TIMER,
			addrmask => CFG_MADR_TIMER
		)
		port map(
			clk       => clk,
			rst       => rst,
			interrupt => interrupt,
			wslvi     => slvi,
			wslvo     => slvo
		);

	clk_gen : process
	begin
		clk <= not clk;
		wait for CLK_PERIOD / 2;
	end process clk_gen;

	rst_stimuli : process
	begin
		rst <= '1';
		wait for CLK_PERIOD;
		rst <= '0';
		wait for 20000 * CLK_PERIOD;
	end process rst_stimuli;

	write_stimuli : process
	begin
		wait for 2.5 * CLK_PERIOD;
		writedata  <= X"00000010";
		addroffset <= 0;
		wait for 3 * CLK_PERIOD;
		writedata  <= X"00000004";
		addroffset <= 4;
		wait for 0.5 * CLK_PERIOD;
	end process write_stimuli;
	
	gen_proc : process
	begin
		generate_sync_wb_single_write(msto, slvo, clk, writedata, "10", addroffset);
	end process gen_proc;
END ARCHITECTURE;