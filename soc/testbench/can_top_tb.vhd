library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lt16soc_peripherals.all;

entity can_top_tb is
	constant Period    : time := 10 ns; -- 100 MHz System Clock
	constant BitPeriod : time := 60 us; -- Kbd Clock is 16.7 kHz max
end entity can_top_tb;

architecture RTL of can_top_tb is
	signal clk : std_logic := '0';
	signal rst : std_logic;

	signal ps2_keyb_clk_1  : std_logic := '1';
	signal ps2_keyb_data_1 : std_logic := '1';
	signal dataLCD_1       : std_logic_vector(7 downto 0);
	signal enableLCD_1     : std_logic;
	signal rsLCD_1         : std_logic;
	signal rwLCD_1         : std_logic;
	signal sw_1            : std_logic_vector(7 downto 0);
	signal btn_1           : std_logic_vector(6 downto 0);
	signal led_1           : std_logic_vector(7 downto 0);
	signal driver_en_1     : std_logic;
	signal receive_en_1    : std_logic;
	signal rxd1_1          : std_logic;
	signal txd1_1          : std_logic;

	signal ps2_keyb_clk_2  : std_logic := '1';
	signal ps2_keyb_data_2 : std_logic := '1';
	signal dataLCD_2       : std_logic_vector(7 downto 0);
	signal enableLCD_2     : std_logic;
	signal rsLCD_2         : std_logic;
	signal rwLCD_2         : std_logic;
	signal sw_2            : std_logic_vector(7 downto 0);
	signal btn_2           : std_logic_vector(6 downto 0);
	signal led_2           : std_logic_vector(7 downto 0);
	signal driver_en_2     : std_logic;
	signal receive_en_2    : std_logic;
	signal rxd1_2          : std_logic;
	signal txd1_2          : std_logic;

--	signal Succeeded : boolean := true;

	type Code_r is record
		Cod : std_logic_vector(7 downto 0);
		Err : Std_logic;                -- note: '1' <=> parity error
	end record;

	type Codes_Table_t is array (natural range <>) of Code_r;

	-- if you need more codes: just add them!
	constant Codes_Table : Codes_Table_t := (
		-- press and unpress key a
		(x"1C", '0'),
		(x"F0", '0'),
		(x"1C", '0'),
		
		-- press and unpress key b
		(x"32", '0'),
		(x"F0", '0'),
		(x"32", '0'),
		
		-- press and unpress key h
		(x"33", '0'),
		(x"F0", '0'),
		(x"33", '0'),
		
		-- press and unpress left arrow key
		(x"E0", '0'),
		(x"6B", '0'),
		(x"E0", '0'),
		(x"F0", '0'),
		(x"6B", '0'),
		
		-- press and unpress left arrow key
		(x"E0", '0'),
		(x"6B", '0'),
		(x"E0", '0'),
		(x"F0", '0'),
		(x"6B", '0'),
		
		-- press and unpress key h
		(x"33", '0'),
		(x"F0", '0'),
		(x"33", '0')
	);

	--	constant Codes_Table : Codes_Table_t := ((x"12", '0'), (x"F0", '0'), (x"12", '0'));

	-- in Verilog, the function below is just : ^V ;-)
	function Even(V : std_logic_vector) return std_logic is
		variable p : std_logic := '0';
	begin
		for i in V'range loop
			p := p xor V(i);
		end loop;
		return p;
	end function;

begin
	-- System Clock & Reset
	clk <= not clk after (Period / 2);
	rst <= '0', '1' after Period;

	rxd1_1 <= txd1_1 and txd1_2;
	rxd1_2 <= txd1_1 and txd1_2;

	top2 : lt16soc_top
		generic map(
			programfilename => "programs/project.ram"
		)
		port map(
			clk           => clk,
			rst           => rst,
			led           => led_2,
			sw            => sw_2,
			btn           => btn_2,
			dataLCD       => dataLCD_2,
			enableLCD     => enableLCD_2,
			rsLCD         => rsLCD_2,
			rwLCD         => rwLCD_2,
			rxd1          => rxd1_2,
			txd1          => txd1_2,
			driver_en     => driver_en_2,
			receive_en    => receive_en_2,
			ps2_keyb_clk  => ps2_keyb_clk_2,
			ps2_keyb_data => ps2_keyb_data_2
		);

	top1 : lt16soc_top
		generic map(
			programfilename => "programs/project.ram"
		)
		port map(
			clk           => clk,
			rst           => rst,
			led           => led_1,
			sw            => sw_1,
			btn           => btn_1,
			dataLCD       => dataLCD_1,
			enableLCD     => enableLCD_1,
			rsLCD         => rsLCD_1,
			rwLCD         => rwLCD_1,
			rxd1          => rxd1_1,
			txd1          => txd1_1,
			driver_en     => driver_en_1,
			receive_en    => receive_en_1,
			ps2_keyb_clk  => ps2_keyb_clk_1,
			ps2_keyb_data => ps2_keyb_data_1
		);

	Emit : process
		procedure SendCode(D   : std_logic_vector(7 downto 0);
				           Err : std_logic := '0') is
		begin
			ps2_keyb_clk_1  <= '1';
			ps2_keyb_data_1 <= '1';
			-- (1) verify that Clk was Idle (high) at least for 50 us.
			-- this is not coded here.
			wait for (BitPeriod / 2);

			-- Start bit
			ps2_keyb_data_1 <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1 <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1 <= '1';

			-- Data Bits
			for i in 0 to 7 loop
				ps2_keyb_data_1 <= D(i);
				wait for (BitPeriod / 2);
				ps2_keyb_clk_1 <= '0';
				wait for (BitPeriod / 2);
				ps2_keyb_clk_1 <= '1';
			end loop;

			-- Odd Parity bit
			ps2_keyb_data_1 <= Err xor not Even(D);
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1 <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1 <= '1';

			-- Stop bit
			ps2_keyb_data_1 <= '1';
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1 <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk_1  <= '1';
			ps2_keyb_data_1 <= '1';
			wait for (BitPeriod * 3);
		end procedure SendCode;
	begin                               -- process Emit
		-----
		Wait for BitPeriod;
		-- Send the Test Frames
		for i in Codes_Table'range loop
			SendCode(Codes_Table(i).Cod, Codes_Table(i).Err);
		end loop;
		--		SendCode(x"1C", '0');
		--		if not Succeeded then
		--			report "End of simulation : " & Lf & " There have been errors in the Data / Err read !" severity failure;
		--		else
		--			report Lf & " SUCCESSFULL End of simulation : " & Lf & " There has been no (zero) error !" & Lf & Ht severity note;
		--			report "End of Simulation" severity failure;
		--		end if;
		wait for 200000000 * Period;
	end process Emit;
end architecture RTL;