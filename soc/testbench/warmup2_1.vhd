-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;

ENTITY switch_tb IS
END ENTITY;

ARCHITECTURE sim OF switch_tb IS
	constant CLK_PERIOD : time := 10 ns;

	signal clk : std_logic := '0';
	signal rst : std_logic;

	signal btn_sw   : std_logic_vector(14 downto 0);
	signal readdata : std_logic_vector(WB_PORT_SIZE - 1 downto 0);

	signal slvi : wb_slv_in_type;
	signal slvo : wb_slv_out_type;
	signal msto : wb_mst_out_type;

BEGIN
	slvi.adr <= msto.adr;
	slvi.dat <= msto.dat;
	slvi.bte <= msto.bte;
	slvi.we  <= msto.we;
	slvi.sel <= msto.sel;
	slvi.stb <= msto.stb;
	slvi.cyc <= msto.cyc;
	slvi.cti <= msto.cti;

	SIM_SLV : wb_switch
		generic map(
			memaddr  => CFG_BADR_SWITCH,
			addrmask => CFG_MADR_SWITCH
		)
		port map(
			clk   => clk,
			rst   => rst,
			btn   => btn_sw(14 downto 8),
			sw    => btn_sw(7 downto 0),
			wslvi => slvi,
			wslvo => slvo
		);

	clk_gen : process
	begin
		clk <= not clk;
		wait for CLK_PERIOD / 2;
	end process clk_gen;

	stimuli : process
	begin
		rst <= '1';
		wait for CLK_PERIOD;
		rst <= '0';
		for i in 0 to 2 ** 15 - 1 loop
			btn_sw <= std_logic_vector(to_unsigned(i, 15));
			wait for 4 * CLK_PERIOD;
		end loop;
	--		wait for (2**15 -1)*3*CLK_PERIOD;
	end process stimuli;

	generate_sync_wb_single_read(msto, slvo, clk, readdata);

END ARCHITECTURE;
