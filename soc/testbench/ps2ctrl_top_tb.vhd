library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Numeric_std.all;
use IEEE.Std_Logic_Textio.all;
use STD.Textio.all;
use work.lt16soc_peripherals.all;
use work.config.all;
use work.wishbone.all;

entity ps2ctrl_top_tb is
	constant Period    : time := 10 ns; -- 100 MHz System Clock
	constant BitPeriod : time := 60 us; -- Kbd Clock is 16.7 kHz max
end;

architecture Test of ps2ctrl_top_tb is
	signal clk           : std_logic := '0';
	signal rst           : std_logic;
	signal ps2_keyb_clk  : std_logic := '1';
	signal ps2_keyb_data : std_logic := '1';
--	signal ascii_new     : std_logic;
	signal Succeeded     : boolean   := true;
--	signal wslvi         : wb_slv_in_type;
--	signal wslvo         : wb_slv_out_type;
	signal dataLCD       : std_logic_vector(7 downto 0);
	signal enableLCD     : std_logic;
	signal rsLCD         : std_logic;
	signal rwLCD         : std_logic;
--	signal sw            : std_logic_vector(7 downto 0);
--	signal btn           : std_logic_vector(6 downto 0);
	signal led           : std_logic_vector(7 downto 0);

	type Code_r is record
		Cod : std_logic_vector(7 downto 0);
		Err : Std_logic;                -- note: '1' <=> parity error
	end record;
	type Codes_Table_t is array (natural range <>) of Code_r;

	-- if you need more codes: just add them!
	constant Codes_Table : Codes_Table_t := (
		(x"12", '0'), (x"12", '0'), (x"1C", '0'),
		(x"1C", '0'), (x"F0", '0'), (x"1C", '0'), (x"12", '0'), (x"F0", '0'), (x"12", '0'),
		(x"1C", '0'),
		(x"1C", '0'), (x"F0", '0'), (x"1C", '0'),
		(x"12", '0'), (x"12", '0'), (x"1C", '0'),
		(x"1C", '0'),
		(x"32", '0'),
		(x"32", '0'),
		(x"21", '0'),
		(x"21", '0'), (x"F0", '0'), (x"21", '0'),
		(x"32", '0'), (x"F0", '0'), (x"32", '0'),
		(x"1C", '0'), (x"F0", '0'), (x"1C", '0'), (x"12", '0'), (x"F0", '0'), (x"12", '0')
	);

	--	constant Codes_Table : Codes_Table_t := ((x"12", '0'), (x"F0", '0'), (x"12", '0'));

	-- in Verilog, the function below is just : ^V ;-)
	function Even(V : std_logic_vector) return std_logic is
		variable p : std_logic := '0';
	begin
		for i in V'range loop
			p := p xor V(i);
		end loop;
		return p;
	end function;

begin

	-- Instanciate the UUT (PS/2 Controller) :
	--	UUT : wb_ps2
	--		generic map(
	--			memaddr  => CFG_BADR_PS2CTRL,
	--			addrmask => CFG_MADR_PS2CTRL
	--		)
	--		port map(
	--			clk           => clk,
	--			rst           => rst,
	--			wslvi         => wslvi,
	--			wslvo         => wslvo,
	--			ascii_new     => ascii_new,
	--			ps2_keyb_clk  => ps2_keyb_clk,
	--			ps2_keyb_data => ps2_keyb_data
	--		);

	TOP : lt16soc_top
		generic map(
			programfilename => "programs/assignment4_ps2.ram"
		)
		port map(
			clk           => clk,
			rst           => rst,
			led           => led,
			sw            => open,
			btn           => open,
			dataLCD       => dataLCD,
			enableLCD     => enableLCD,
			rsLCD         => rsLCD,
			rwLCD         => rwLCD,
			ps2_keyb_clk  => ps2_keyb_clk,
			ps2_keyb_data => ps2_keyb_data,
			driver_en     => open,
			receive_en    => open,
			rxd1          => open,
			txd1          => open
		);

	-- System Clock & Reset
	clk <= not clk after (Period / 2);
	rst <= '0', '1' after Period;

	-- Keyboard sending Data to the Controller
	Emit : process
		procedure SendCode(D   : std_logic_vector(7 downto 0);
				           Err : std_logic := '0') is
		begin
			ps2_keyb_clk  <= '1';
			ps2_keyb_data <= '1';
			-- (1) verify that Clk was Idle (high) at least for 50 us.
			-- this is not coded here.
			wait for (BitPeriod / 2);

			-- Start bit
			ps2_keyb_data <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk <= '1';

			-- Data Bits
			for i in 0 to 7 loop
				ps2_keyb_data <= D(i);
				wait for (BitPeriod / 2);
				ps2_keyb_clk <= '0';
				wait for (BitPeriod / 2);
				ps2_keyb_clk <= '1';
			end loop;

			-- Odd Parity bit
			ps2_keyb_data <= Err xor not Even(D);
			wait for (BitPeriod / 2);
			ps2_keyb_clk <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk <= '1';

			-- Stop bit
			ps2_keyb_data <= '1';
			wait for (BitPeriod / 2);
			ps2_keyb_clk <= '0';
			wait for (BitPeriod / 2);
			ps2_keyb_clk  <= '1';
			ps2_keyb_data <= '1';
			wait for (BitPeriod * 3);
		end procedure SendCode;
	begin                               -- process Emit
		-----
		Wait for BitPeriod;
		-- Send the Test Frames
		for i in Codes_Table'range loop
			SendCode(Codes_Table(i).Cod, Codes_Table(i).Err);
		end loop;
		if not Succeeded then
			report "End of simulation : " & Lf & " There have been errors in the Data / Err read !" severity failure;
		else
			report Lf & " SUCCESSFULL End of simulation : " & Lf & " There has been no (zero) error !" & Lf & Ht severity note;
			report "End of Simulation" severity failure;
		end if;
	end process Emit;

-- Host reading (& verifying) Data :
--	Host : process
--		variable L     : line;
--		variable Index : natural := 0;
--	begin
--		wait until Scan_DAV = '1';
--		wait for 300 * Period;
--		DoRead <= '1';
--		write(L, now, right, 12);
--		write(L, Ht & "Scan code read (hex) = ");
--		hwrite(L, Scan_Code);
--		if Scan_Err = '1' then
--			write(L, ht & " >>> Scan_Err <<<");
--		end if;
--		-- Compare with the original Data-Error :
--		if (Scan_Code /= Codes_Table(Index).Cod) or (Scan_Err /= Codes_Table(Index).Err) then
--			Succeeded <= False;
--			write(L, Ht & "!!! Mismatch !!!");
--		end if;
--		Index := Index + 1;
--		writeline(output, L);
--		wait for Period;
--		DoRead <= '0';
--	end process Host;

end Test;